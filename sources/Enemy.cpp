
#include "Enemy.h"
#include <Framework.h>
#include <Vector3DCompressor.h>
#include <IntCompressor.h>
#include <QuaternionCompressor.h>
#include <FloatCompressor.h>

// Getters / Setters
namespace uqac::game
{
    void Enemy::WritePosition(uqac::serializer::Serializer *s)
    {
        uqac::serializer::Vector3D<float> v_min = uqac::serializer::Vector3D<float>::Vector3D(-500, -500, 0);
        uqac::serializer::Vector3D<float> v_max = uqac::serializer::Vector3D<float>::Vector3D(500, 500, 100);
        uqac::serializer::Vector3D<int> v_precision = uqac::serializer::Vector3D<int>::Vector3D(3, 3, 3);
        uqac::serializer::Vector3DCompressor comp_pos = uqac::serializer::Vector3DCompressor::Vector3DCompressor(v_min, v_max, v_precision);
        comp_pos.Compress(s, m_position);
    }

    void Enemy::WriteRotation(uqac::serializer::Serializer *s)
    {
        uqac::serializer::QuaternionCompressor comp_rot = uqac::serializer::QuaternionCompressor::QuaternionCompressor();
        comp_rot.Compress(s, m_rotation);
    }

    uqac::serializer::Vector3D<float> Enemy::ReadPosition(uqac::serializer::Deserializer *des)
    {
        uqac::serializer::Vector3D<float> v_min = uqac::serializer::Vector3D<float>::Vector3D(-500, -500, 0);
        uqac::serializer::Vector3D<float> v_max = uqac::serializer::Vector3D<float>::Vector3D(500, 500, 100);
        uqac::serializer::Vector3D<int> v_precision = uqac::serializer::Vector3D<int>::Vector3D(3, 3, 3);
        uqac::serializer::Vector3DCompressor decomp_pos = uqac::serializer::Vector3DCompressor::Vector3DCompressor(v_min, v_max, v_precision);
        return decomp_pos.Decompress(des);
    }

    uqac::serializer::Quaternion Enemy::ReadRotation(uqac::serializer::Deserializer *des)
    {
        uqac::serializer::QuaternionCompressor decomp_rot = uqac::serializer::QuaternionCompressor::QuaternionCompressor();
        return decomp_rot.Decompress(des);
    }

    void Enemy::Read(uqac::serializer::Deserializer *des)
    {
        m_position = ReadPosition(des);
        m_rotation = ReadRotation(des);
        uqac::serializer::IntCompressor vie_decomp = uqac::serializer::IntCompressor::IntCompressor(0, 300);
        m_vie = vie_decomp.Decompress(des);
    }

    void Enemy::Write(uqac::serializer::Serializer *s)
    {
        WritePosition(s);
        WriteRotation(s);
        uqac::serializer::IntCompressor vie_comp = uqac::serializer::IntCompressor::IntCompressor(0, 300);
        vie_comp.Compress(s, m_vie);
    }

    uint32_t Enemy::GetClassId()
    {
        return (uint32_t) ClassID;
    }

    uint32_t Enemy::GetStaticClassId()
    {
        return (uint32_t) ClassID;
    }

    EnemyType Enemy::getEnemyType()
    {
        return m_enemyType;
    }

    void Enemy::Print()
    {
        std::cout << "Enemy's position: " << m_position.x << " " << m_position.y << " " << m_position.z << std::endl;
    }
}