#include "LinkingContext.h"
#include <iostream>

namespace uqac::replication
{
    using NetworkId = uint32_t;

    void LinkingContext::addToContext(uqac::game::NetworkObject *oPointeur, NetworkId networkId)
    {
        m_idToPointeur.insert(std::make_pair(networkId, oPointeur));
        m_pointeurToId.insert(std::make_pair(oPointeur, networkId));
        if (networkId > m_firstFreeId)
        {
            m_firstFreeId = networkId + 1;
        }
    }

    void LinkingContext::addToContext(uqac::game::NetworkObject *oPointeur)
    {
        if (m_pointeurToId.count(oPointeur) == 0)
        {
            NetworkId networkId = m_firstFreeId;
            addToContext(oPointeur, networkId);
            m_firstFreeId++;
        } else 
        {
            std::cout << "Already in context" << std::endl;
        }
    }

    void LinkingContext::removeFromContext(uqac::game::NetworkObject *oPointeur)
    {

        if (m_pointeurToId.count(oPointeur))
        {
            m_pointeurToId.erase(oPointeur);
        }
        else
        {
            std::cout << "Nothing to erase in m_pointeurToId" << std::endl;
        }

        std::optional<int> i = getIdFromObject(oPointeur);

        if (i.has_value())
        {
            m_idToPointeur.erase(*i);
        }
        else
        {
            std::cout << "Nothing to erase in m_idToPointeur" << std::endl;
        }
    }

    std::optional<NetworkId> LinkingContext::getIdFromObject(uqac::game::NetworkObject *oPointeur)
    {
        if (m_pointeurToId.count(oPointeur))
        {
            return m_pointeurToId[oPointeur];
        }
        return std::nullopt;
    }

    std::optional<uqac::game::NetworkObject *> LinkingContext::getObjectFromId(NetworkId oId)
    {
        if (m_idToPointeur.count(oId))
        {
            return m_idToPointeur[oId];
        }
        return std::nullopt;
    }
}