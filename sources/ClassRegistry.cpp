#include "ClassRegistry.h"
#include "Player.h"
#include "Enemy.h"
namespace uqac::replication
{
    uqac::game::NetworkObject* ClassRegistry::Create(uint32_t classID)
    {
        return getInstance().m_functionMap[classID]();
    }
}