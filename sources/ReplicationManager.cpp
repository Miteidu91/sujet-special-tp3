#include "ReplicationManager.h"
#include "Serializer.h"
#include "NetworkObject.h"
namespace uqac::replication
{
    void ReplicationManager::Update(uqac::serializer::Serializer* s, uqac::serializer::Deserializer* d)
    {
        SerializeWorld(s);
        DeserializeWorld(d);
    }

    void ReplicationManager::SerializeWorld(uqac::serializer::Serializer* s)
    {
        std::vector<uqac::game::NetworkObject *> world{m_networkObjectSet.begin(), m_networkObjectSet.end()};
        for (uqac::game::NetworkObject* netObj : world)
        {
            if(netObj != nullptr)
                SerializeNetworkObj(s, netObj);
        }
    }

    void ReplicationManager::DeserializeWorld(uqac::serializer::Deserializer* d)
    {
        while(!(d->isCompletelyRead()))
        {
            DeserializeNetworkObj(d);
        }
    }

    void ReplicationManager::SerializeNetworkObj(uqac::serializer::Serializer* s, uqac::game::NetworkObject* p_networkObject)
    {
        //Network ID
        std::optional<uint32_t> netID = m_linkingContext.getIdFromObject(p_networkObject);
        s->Serialize<uint32_t>(netID.value());
        
        // Class ID
        uint32_t classID = p_networkObject->GetClassId();
        s->Serialize<uint32_t>(classID);

        //Networkbject
        p_networkObject->Write(s);
    }

    void ReplicationManager::DeserializeNetworkObj(uqac::serializer::Deserializer* d)
    {
        uint32_t netID = d->Deserialize<uint32_t>();
        uint32_t classID = d->Deserialize<uint32_t>();

        std::optional< uqac::game::NetworkObject*> p_networkObject_tmp = m_linkingContext.getObjectFromId(netID);
        uqac::game::NetworkObject* p_networkObject_ptr;
        if (!p_networkObject_tmp.has_value())
        {
            //try {
                p_networkObject_ptr = ClassRegistry::getInstance().Create(classID);
                m_linkingContext.addToContext(p_networkObject_ptr);
            /*}
            catch (std::exception& e)
            {
                std::cerr << "[E] Error while creating object: " << classID << " | " << e.what() << std::endl;
                return;
            }*/
        } else
        {
            p_networkObject_ptr = p_networkObject_tmp.value();
        }
        p_networkObject_ptr->Read(d);
        m_networkObjectSet.insert(p_networkObject_ptr);
    }
}