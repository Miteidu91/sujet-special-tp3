#pragma once

#include "NetworkObject.h"
#include <map>
#include <optional>

namespace uqac::replication
{
    class LinkingContext
    {
    public :
        using NetworkId = uint32_t;
        LinkingContext() = default;
        ~LinkingContext() = default;

        void addToContext(uqac::game::NetworkObject *oPointeur, NetworkId networkId);
        void addToContext(uqac::game::NetworkObject *oPointeur);

        void removeFromContext(uqac::game::NetworkObject *oPointeur);

        std::optional<NetworkId> getIdFromObject(uqac::game::NetworkObject *oPointeur);
        std::optional<uqac::game::NetworkObject *> getObjectFromId(NetworkId oId);

    private:
        std::map<NetworkId, uqac::game::NetworkObject *> m_idToPointeur;
        std::map<uqac::game::NetworkObject *, NetworkId> m_pointeurToId;
        static inline NetworkId m_firstFreeId = 0;
    };
}