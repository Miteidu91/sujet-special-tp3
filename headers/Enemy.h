#pragma once

#include "NetworkObject.h"
#include <Framework.h>

namespace uqac::game
{
    enum EnemyType
    {
        Boss = 1,
        Sbire = 2
    };

    class Enemy : public NetworkObject
    {
        private:
            uqac::serializer::Vector3D<float> m_position;
            uqac::serializer::Quaternion m_rotation;
            int m_vie;
            EnemyType m_enemyType;
            void WritePosition(uqac::serializer::Serializer *s);
            void WriteRotation(uqac::serializer::Serializer *s);
            void WriteType(uqac::serializer::Serializer *s);
            void WriteClassID(uqac::serializer::Serializer *s);
            int ReadClassID(uqac::serializer::Deserializer *des);
            uqac::serializer::Quaternion ReadRotation(uqac::serializer::Deserializer *des);
            uqac::serializer::Vector3D<float> ReadPosition(uqac::serializer::Deserializer *des);

        public:
            enum { ClassID = 'ENEM' };

            Enemy(uqac::serializer::Vector3D<float> position, uqac::serializer::Quaternion rotation, int vie, EnemyType enemyType) : m_position(position), m_rotation(rotation), m_vie(vie), m_enemyType(enemyType)
            {
            }
            ~Enemy()
            {
            }

            void Write(uqac::serializer::Serializer *s) override;
            void Read(uqac::serializer::Deserializer *d) override;
            static uint32_t GetStaticClassId();
            uint32_t GetClassId() override;
            //void Destroy() override;
            EnemyType getEnemyType();
            void Print() override;
    };
}