#pragma once

#include "NetworkObject.h"
#include <iostream>
#include <map>
#include <functional>

namespace uqac::replication
{
    class ClassRegistry {
        private :
            ClassRegistry(){};
        public:
            std::map<uint32_t, std::function<uqac::game::NetworkObject*(void)>> m_functionMap;//a repasser en private
            
            static ClassRegistry& getInstance()
            {
                static ClassRegistry instance;
                return instance;
            }
            ClassRegistry(ClassRegistry const&) = delete;
            void operator=(ClassRegistry const&)  = delete;
            static uqac::game::NetworkObject* Create(uint32_t classID);

            template <typename T>
            static void Register(std::function<uqac::game::NetworkObject *(void)> classFunction)
            {
                getInstance().m_functionMap[T::GetStaticClassId()] = classFunction;
                //std::cout << "Registered class " << T::GetStaticClassId() << std::endl;
            }
    };
}

