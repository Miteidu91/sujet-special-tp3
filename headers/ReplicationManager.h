#pragma once
#include <Serializer.h>
#include <Deserializer.h>
#include "ClassRegistry.h"
#include "LinkingContext.h"
#include <unordered_set>
#include <vector>

namespace uqac::replication
{
    /*enum class PacketType : uint8_t {
        Hello = 0x00,
        Sync = 0x01,
        Bye = 0x02,
        PacketType_Max
    }; peut etre necessaire pour la suite*/


    class ReplicationManager
    {
        private:
            uqac::replication::LinkingContext m_linkingContext;

            void SerializeNetworkObj(uqac::serializer::Serializer* s, uqac::game::NetworkObject* p_networkObject);
            void DeserializeNetworkObj(uqac::serializer::Deserializer* d);

        public:
            std::unordered_set<uqac::game::NetworkObject*> m_networkObjectSet;
            ReplicationManager(uqac::replication::LinkingContext linkingContext_in) 
                : m_linkingContext(linkingContext_in) {};
            ~ReplicationManager()
            {
                for (uqac::game::NetworkObject *networkObjectptr : m_networkObjectSet)
                {
                    if(networkObjectptr != nullptr)
                    {
                        delete networkObjectptr;
                    }
                }
            }

            template <typename T>
            void Create()
            {
                uqac::game::NetworkObject *p_networkObject = ((ClassRegistry::getInstance()).Create(T::GetStaticClassId()));
                m_linkingContext.addToContext(p_networkObject);
                m_networkObjectSet.insert(p_networkObject);
            }
            
            void Update(uqac::serializer::Serializer* s, uqac::serializer::Deserializer* d);
            void SerializeWorld(uqac::serializer::Serializer* s);
            void DeserializeWorld(uqac::serializer::Deserializer* d);
    };
}