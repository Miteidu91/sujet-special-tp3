#pragma once
#include <Serializer.h>
#include <Deserializer.h>

namespace uqac::game
{    
    class NetworkObject {
        public:
            virtual void Write(uqac::serializer::Serializer *s) = 0;
            virtual void Read(uqac::serializer::Deserializer *d) = 0;
            virtual uint32_t GetClassId() = 0; //vient de là
            //virtual void Destroy() = 0;
            virtual void Print() = 0;
    };
}