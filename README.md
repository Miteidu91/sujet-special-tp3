# Sujet Spécial TP3

TP3 sujet spécial en informatique II

## Membres du groupe :

* Basile Bonicel
* Mathias Durand
* Théo Loubet

## Informations

Pour tester :
* Lancer un build sur toutes les règles
* Lancer un serveur : `.\Server.exe 56783` par exemple
* Lancer un ou plusieurs clients : `.\Client.exe localhost 56783` par exemple
* Toutes les 3 secondes le serveur envoie les données du monde aux joueurs (nous n'avons pas pu intégrer la mise à jour aléatoire des données sur les networkObject, seulement un random de l'argent du player : `m_argent = rand() % (23459 - (-500) + 1) + (-500) +0.5f;` dans la fonction Write de Player.cpp. Hors du cadre de cet exemple cette ligne n'existerait bien évidemment pas).
* Au deuxième joueur connecté, un ennemi est créé (lorsque le monde est affiché à l'écran, seule sa position l'est)
* (La déconnection ne supprime pour l'instant pas le player correspondant)
* Dans quelques rares essais, un "abort()" est survenu. Généralement, relancer le serveur ou recompiler le projet résout le problème. Il semblerait que des connexions de clients trop raprochées sont la cause de l'erreur mais après plusieurs heures de debug nous ne sommes pas parvenus à trouver la ligne responsable du bug. Nous avons réalisé une vidéo des serveurs et clients lorsqu'ils fonctionnent : []()https://youtu.be/_0gCj-F4G5U
* Pour voir le serveur et les clients qui fonctionnent avec randomisation de l'argent : []()https://youtu.be/lnSl41yjhUE
