#define WIN32_LEAN_AND_MEAN

#include <Connection.hpp>
#include <Berkeley.hpp>
#include "LinkingContext.h"
#include "ReplicationManager.h"
#include "Player.h"
#include "Enemy.h"
#include "Utils.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <string>

#include <iostream>

void printWorld(uqac::replication::ReplicationManager *rm)
{
    std::cout << "World : " << std::endl;
    for (auto it = rm->m_networkObjectSet.begin(); it != rm->m_networkObjectSet.end(); ++it)
    {
        std::cout << "NetworkObject : " << std::endl;
        (*it)->Print();
        std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << std::endl;
}

void onConnectionLost(uqac::network::Berkeley *berk, std::string coText)
{
    //TODO : a changer pour depop un joueur ?
    std::cout << "[I] Connection avec " << coText << " perdue" << std::endl;
}

int __cdecl main(int argc, char **argv)
{
    if (argc < 2)
    {
        // Validate the parameters
        std::cout << "usage: " << argv[0] << " <port>" << std::endl;
        exit(1);
    }

    uqac::network::ConnectionType protocol = uqac::network::ConnectionType::TCP;
    std::string port = argv[1];
    uqac::replication::LinkingContext lc = uqac::replication::LinkingContext();
    uqac::replication::ReplicationManager* rm = new uqac::replication::ReplicationManager(lc);
    int idPlayer = 1;
    std::function<uqac::game::NetworkObject *(void)> playerFunction = [&idPlayer]()
    {
        std::string name = "TEST" + std::to_string(idPlayer);
        uqac::game::Player *p = (new uqac::game::Player(uqac::serializer::Vector3D<float>(0.0f, 0.0f, 0.0f), uqac::serializer::Quaternion(0.0f, 0.0f, 0.0f, 1.0f), uqac::serializer::Vector3D<float>(1.0f, 2.0f, 3.0f), 100, 50, 120.3f, name));
        idPlayer++;
        return p;
    };
    std::function<uqac::game::NetworkObject *(void)> enemyFunction = []()
    {
        uqac::game::Enemy *p = (new uqac::game::Enemy(uqac::serializer::Vector3D<float>(2.0f, 3.0f, 4.0f), uqac::serializer::Quaternion(1.0f, 0.0f, 0.0f, 0.0f), 4, uqac::game::EnemyType::Boss));
        return p;
    };
    uqac::replication::ClassRegistry::Register<uqac::game::Player>(playerFunction);
    uqac::replication::ClassRegistry::Register<uqac::game::Enemy>(enemyFunction);
    uqac::network::Berkeley berk(
        protocol,
        [](uqac::network::Berkeley *berk, uqac::network::Connection *co, SuperCharStar message_received)
        {
            berk->BroadcastMessageToAllButSender(message_received, co);
        },
        [rm](uqac::network::Berkeley *berk, uqac::network::Connection *co)
        {
            rm->Create<uqac::game::Player>();
            //printWorld(rm);
        },
        onConnectionLost);
    berk.ListenForClient(port);
    berk.Run();
    std::cout << "[I] Serveur operationnel" << std::endl;

    bool notEnemy = true;
    while (true)
    {
        if (berk.GetVectorOfConnectionsSize() == 2 && notEnemy)
        {
            std::cout << "[I] Ajout d'un ennemi" << std::endl;
            rm->Create<uqac::game::Enemy>();
            notEnemy = false;
        }
        //TODO: changer aléatoirement les valeurs des positions etc. des networkObjects
        uqac::serializer::Serializer s = uqac::serializer::Serializer();
        rm->SerializeWorld(&s);
        SuperCharStar message;
        message.data = s.getContainer();
        message.size = s.getContainerSize();
        /*for (int i = 0; i < message.size; i++)
        {
            char c = (message.data[i]) ? message.data[i] : '.';
            std::cout << c;
        }
        std::cout << "|" << message.size << std::endl;*/
        
        berk.BroadcastMessageToAllButSender(message, nullptr);
        printWorld(rm);
        Sleep(3000);//1porten : le sèrv doi pa alé tro vit
    }
    return 0;
}