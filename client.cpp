#define WIN32_LEAN_AND_MEAN

#include <Connection.hpp>
#include <Berkeley.hpp>
#include "Enemy.h"
#include "Player.h"
#include "ReplicationManager.h"
#include "LinkingContext.h"
#include "Utils.hpp"
#include <stdlib.h>
#include <stdio.h>

#include <iostream>

void printWorld(uqac::replication::ReplicationManager *rm)
{
    std::cout << "World : " << std::endl;
    for (auto it = rm->m_networkObjectSet.begin(); it != rm->m_networkObjectSet.end(); ++it)
    {
        std::cout << "NetworkObject : " << std::endl;
        (*it)->Print();
        std::cout << std::endl;
    }
    std::cout << "---------" << std::endl;
}

void onConnectionLost(uqac::network::Berkeley *berk, std::string co)
{
    std::cout << "[I] Connection avec le serveur perdue" << std::endl;
    if (berk->IsVectorOfConnectionsEmpty())
    {
        std::cout << "[I] Fermeture du programme" << std::endl;
        berk->runThread = false;
        exit(0);
    }
}

int __cdecl main(int argc, char **argv)
{
    // Validate the parameters
    if (argc < 3)
    {
        std::cout << "usage: " << argv[0] << " <server ip> <port>" << std::endl;
        exit(1);
    }
    uqac::network::ConnectionType protocol = uqac::network::ConnectionType::TCP;
    std::string ip = argv[1];
    std::string port = argv[2];

    uqac::replication::LinkingContext lc = uqac::replication::LinkingContext();
    uqac::replication::ReplicationManager *rm = new uqac::replication::ReplicationManager(lc);
    std::function<uqac::game::NetworkObject *(void)> playerFunction = []()
    {
        uqac::game::Player *p = (new uqac::game::Player(uqac::serializer::Vector3D<float>(0.0f, 0.0f, 0.0f), uqac::serializer::Quaternion(0.0f, 0.0f, 0.0f, 1.0f), uqac::serializer::Vector3D<float>(1.0f, 2.0f, 3.0f), 100, 50, 120.3f, "vide"));
        return p;
    };
    std::function<uqac::game::NetworkObject *(void)> enemyFunction = []()
    {
        uqac::game::Enemy *p = (new uqac::game::Enemy(uqac::serializer::Vector3D<float>(2.0f, 3.0f, 4.0f), uqac::serializer::Quaternion(1.0f, 0.0f, 0.0f, 0.0f), 4, uqac::game::EnemyType::Boss));
        return p;
    };
    uqac::replication::ClassRegistry::Register<uqac::game::Player>(playerFunction);
    uqac::replication::ClassRegistry::Register<uqac::game::Enemy>(enemyFunction);

    uqac::network::Berkeley berk(
        protocol,
        [rm](uqac::network::Berkeley *berk, uqac::network::Connection *co, SuperCharStar message_received)
        {
            uqac::serializer::Deserializer d = uqac::serializer::Deserializer(message_received.data, message_received.size);
            rm->DeserializeWorld(&d);
            printWorld(rm);
        },
        [rm](uqac::network::Berkeley *berk, uqac::network::Connection *co)
        { 
            std::cout << "Connecte au serveur" << std::endl;
        },
        onConnectionLost);
    uqac::network::Connection *serverConnection = berk.ConnectToServer(ip, port);
    berk.Run();
    while (true)
    {
        Sleep(1);
    }
    return 0;
}